import {
    ProviderState,
    MenuDto,
    SubMenu,
    CarouselContent,
    RadioDto,
    Advantage,
    MapCoordinates,
    Contact,
    SocialNetwork
} from "./types";

import {
    RouteName,
    MenuName,
    CarouselContentEnum,
    CarouselItemsEnum,
    RadioEnum,
    MapCoordinatesEnum,
    ContactHrefDto,
    ContactsDto,
} from "./constants";

import Phone from "../assets/icons/contacts/phone.svg";
import MapPin from "../assets/icons/contacts/map-pin.svg";
import Mail from "../assets/icons/contacts/mail.svg";
import Clock from "../assets/icons/contacts/clock.svg";
import IndividualDesign from "../assets/icons/main/individual-design.svg";
import HighQuality from "../assets/icons/main/high-quality.svg";
import ExperiencedCraftsmen from "../assets/icons/main/experienced-craftsmen.svg";
import FlexiblePrices from "../assets/icons/main/flexible-prices.svg";
import WhatsApp from "../assets/icons/social-networks/whats-app.svg";
import Telegram from "../assets/icons/social-networks/telegram.svg";
import Vk from "../assets/icons/social-networks/vk.svg";

// Provide State

export const providerState: ProviderState = {
    sidebar: false,
    modal: false
}

// Menu

const subMenu:SubMenu[] = [
    {
        id: 21,
        title: "Дизайн интерьера",
        path: "/",
        subMenu: [
            {
                id: 1,
                title: "Дизайн-проект помещения",
                path: "/",
            },
            {
                id: 2,
                title: "3D визуализация",
                path: "/",
            },
            {
                id: 3,
                title: "Подбор стиля и материалов",
                path: "/",
            }
        ]
    },
    {
        id: 22,
        title: "Изготовление мебели",
        path: "/",
        subMenu: [
            {
                id: 1,
                title: "Мягкая мебель",
                path: "/",
            },
            {
                id: 2,
                title: "Корпусная мебель",
                path: "/",
            },
            {
                id: 3,
                title: "Столы и стулья",
                path: "/",
            },
            {
                id: 4,
                title: "Кровати",
                path: "/",
            }
        ]
    },
    {
        id: 23,
        title: "Дополнительные услуги",
        path: "/",
        subMenu: [
            {
                id: 1,
                title: "Доставка и сборка",
                path: "/",
            },
            {
                id: 2,
                title: "Гарантийное обслуживание",
                path: "/",
            },
            {
                id: 3,
                title: "Ремонт и реставрация",
                path: "/",
            },
            {
                id: 4,
                title: "Перетяжка мебели",
                path: "/",
            }
        ]
    }
];
export const itemMenu:MenuDto[] = [
    {
        id: 1,
        title: MenuName.Home,
        path: RouteName.Home
    },
    {
        id: 2,
        title: MenuName.Services,
        path: RouteName.Services,
        subMenu: subMenu
    },
    {
        id: 3,
        title: MenuName.About,
        path: RouteName.About
    },
    {
        id: 4,
        title: MenuName.Reviews,
        path: RouteName.Reviews
    },
    {
        id: 5,
        title: MenuName.Contacts,
        path: RouteName.Contacts
    }
];

// Carousel content

export const carouselContent: CarouselContent = {
    title: CarouselContentEnum.FirstTitle || CarouselContentEnum.SecondTitle,
    description: CarouselContentEnum.Description,
    items: Object.values(CarouselItemsEnum),
    button: CarouselContentEnum.Button
}

// Radio buttons

export const listRadios: RadioDto[] = [
    {
        slide: RadioEnum.FirstSlide,
    },
    {
        slide: RadioEnum.SecondSlide,
    }
];

// Benefit of work with us

export const listAdvantages: Advantage[] = [
    {
        icon: IndividualDesign,
        title: 'Индивидуальный дизайн',
        description: 'Создаем уникальную мебель по вашему вкусу',
    },
    {
        icon: HighQuality,
        title: 'Высокое качество',
        description: 'Используем только лучшие проверенные материалы',
    },
    {
        icon: ExperiencedCraftsmen,
        title: 'Опытные мастера',
        description: 'Команда профессиональных дизайнеров и столяров',
    },
    {
        icon: FlexiblePrices,
        title: 'Гибкие цены',
        description: 'Предлагаем пакеты услуг по разным ценам на любой бюджет',
    }
];

// Maps

export const coordinates: MapCoordinates = {
    zoom: MapCoordinatesEnum.Zoom,
    defaultCoordinates: [MapCoordinatesEnum.LongitudeDefault, MapCoordinatesEnum.LatitudeDefault],
    tabletCoordinates: [MapCoordinatesEnum.LongitudeTablet, MapCoordinatesEnum.LatitudeTablet],
}

export const contacts: Contact[] = [
    {
        img: Phone,
        alt: "телефон для связи",
        tag: "a",
        attr: {
            href: ContactHrefDto.Phone,
        },
        text: ContactsDto.Phone,
        addField: true
    },
    {
        img: MapPin,
        alt: "наш адрес",
        tag: "address",
        attr: null,
        text: ContactsDto.Pin
    },
    {
        img: Mail,
        alt: "почтовый адрес",
        tag: "a",
        attr: {
            href: ContactHrefDto.Mail,
        },
        text: ContactsDto.Mail
    },
    {
        img: Clock,
        alt: "время работы",
        tag: "span",
        attr: null,
        text: ContactsDto.Clock
    }
];

// Modal content

export const modalContent = {
    title: "Закажите звонок",
    description: "Мы свяжемся с вами в ближайшее время и ответим на все вопросы",
    name: "Имя",
    phone: "Телефон*",
    button: "Оставить заявку",
    info: "Отправляя заявку, вы даете согласие на",
    warning: "обработку персональных данных",
    success: "Иконка успешной отправки",
    error: "Иконка негативной отправки",
    successText: "Заявка успешно отправлена",
    errorText: "Заявка не отправлена, попробуйте снова",
}

export const inputName = {
    name: "name",
    phone: "phone",
}

// Social Networks

export const socialNetwork: SocialNetwork[] = [
    {
        link: "https://wa.me/79038010774",
        icon: WhatsApp,
        text: "WhatsApp иконка"
    },
    {
        link: "https://telegram.me/andyriga",
        icon: Telegram,
        text: "Telegram иконка"
    },
    {
        link: "https://vk.com/andy_riga",
        icon: Vk,
        text: "Иконка VK"
    },
];
