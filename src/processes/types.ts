import {
    ReactNode,
    Dispatch,
    ChangeEvent,
    MouseEventHandler
} from "react";

import {
    RouteName,
    MenuName,
    CarouselContentEnum,
    CarouselItemsEnum,
    MapCoordinatesEnum,
    ContactHrefDto,
    ContactsDto,
    ModalStatus
} from "./constants";

// Provide State

export interface ProviderState {
    sidebar: boolean;
    modal: boolean;
}

// Wrapper

export interface WrapperProps {
    isLogo?: boolean;
    open: boolean;
    children?: ReactNode;
}

// Logo

export interface LogoProps {
    small?: boolean;
}

// Phone Link

export interface PhoneLinkProps {
    className?: string;
}

// Menu

export interface ItemSubMenu {
    id: number;
    title: string;
    path: string;
}

export interface SubMenu extends ItemSubMenu {
    subMenu: ItemSubMenu[];
}

export interface MenuDto {
    id: number;
    title: MenuName;
    path: RouteName;
    subMenu?: SubMenu[];
}

export interface MenuProps {
    menu: SubMenu[];
}

// Carousel content

export interface CarouselContent {
    title: CarouselContentEnum.FirstTitle | CarouselContentEnum.SecondTitle;
    description: CarouselContentEnum.Description;
    items: CarouselItemsEnum[];
    button: CarouselContentEnum.Button;
}

// Radio Button

export interface RadioDto {
    slide: string;
    disabled?: boolean;
}

export interface RadioProps {
    radios: RadioDto[];
    selected: RadioDto;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
}

// Input

export interface InputProps {
    type?: "text" | "tel";
    value: string;
    name: string;
    placeholder?: string;
    required?: boolean;
    error?: boolean;
    full?: boolean;
    className?: string;
    disabled?: boolean;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}

// Button

export interface ButtonProps {
    children?: ReactNode;
    disabled?: boolean;
    type?: "button" | "submit";
    small?: boolean;
    ghost?: boolean;
    modal?: boolean;
    fullWidth?: boolean;
    className?: string;
    onClick?: MouseEventHandler<HTMLButtonElement>;
}

// Close

export interface CloseProps {
    className?: string;
}

// Triangle

export interface TriangleProps {
    rotate?: number;
    className?: string;
}

// Icon

export interface IconProps {
    icon: string;
    title?: string;
    circle?: boolean;
    rounded?: boolean;
    square?: boolean;
    rotate?: number;
    className?: string;
    disabled?: boolean;
    onClick?: MouseEventHandler<HTMLButtonElement>;
}

// Benefit of work with us

export interface Advantage {
    icon: string;
    title: string;
    description: string;
}

// Blocks

export interface BlockProps {
    icon: string;
    title: string;
    description: string;
}

// Maps

export interface MapCoordinates {
    zoom: number;
    defaultCoordinates: MapCoordinatesEnum[],
    tabletCoordinates: MapCoordinatesEnum[],
}

export interface MapState {
    center: number[];
    zoom: number;
}

interface Attr {
    href: ContactHrefDto;
}
export interface Contact {
    img: string;
    alt: string;
    tag: string;
    attr: Attr | null;
    text: ContactsDto;
    addField?: boolean;
}

// Modal fields

export interface ModalProps {
    // @ts-ignore
    handleStatus: (status: ModalStatus.Success | ModalStatus.Error | null) => Dispatch;
}

export interface FormContact {
    name: string;
    phone: string;
}

// Social Networks

export interface SocialNetwork {
    link: string;
    icon: string;
    text: string;
}
