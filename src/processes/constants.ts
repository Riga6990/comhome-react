export enum RouteName {
    Home = "/",
    Services = "services",
    About = "about",
    Reviews = "reviews",
    Contacts = "contacts"
}

export enum MenuName {
    Home = "Логотип",
    Services = "Услуги",
    About = "О компании",
    Reviews = "Отзывы",
    Contacts = "Контакты"
}

// Carousel

export enum CarouselContentEnum {
    FirstTitle = "COMHOME – создадим мебель вашей мечты",
    SecondTitle = "Скидка 20% на первый заказ",
    Description = "Эксклюзивная мебель любой сложности полностью по вашему вкусу",
    Button = "Оставить заявку"
}

export enum CarouselItemsEnum {
    IndividualDesign = "Индивидуальный дизайн",
    HighQualityMaterials = "Высокое качество материалов",
    ProductWarranty = "Гарантия на изделия"
}

export enum RadioEnum {
    FirstSlide = "slide-1",
    SecondSlide = "slide-2"
}

// Map coordinates

export enum MapCoordinatesEnum {
    Zoom = 16,

    // Долгота и широта для карты под десктоп и мобилу (а также располжение пина)
    LongitudeDefault = 55.759362,
    LatitudeDefault = 37.638614,

    // Долгота и широта для карты под плашет
    LongitudeTablet = 55.759524,
    LatitudeTablet = 37.634700
}

export enum ContactHrefDto {
    Phone = "tel:+79038010774",
    Mail = "mailto:riga6990@mail.ru",
}

export enum ContactsDto {
    Phone = "+ 7 (903) 801 07-74",
    Pin = "г. Москва, Девяткин переулок, д. 4",
    Mail = "riga6990@mail.ru",
    Clock = "Будни с 9:00 до 18:00",
}

// Modal status

export enum ModalStatus {
    Success = "success",
    Error = "error",
}
