import "./BenefitsOfWork.scss";
import Block from "../../entities/main/block/Block";
import { listAdvantages } from "../../processes/data";
import { Advantage } from "../../processes/types";

export default function BenefitsOfWork(): JSX.Element {
    const advantages: JSX.Element[] = listAdvantages.map((advantage: Advantage) => {
        return <Block {...advantage} key={advantage.title} />
    });

    return (
        <section className="benefit-of-work container">
            <h3 className="benefit-of-work__title">Преимущества работы с нами</h3>
            <div className="benefit-of-work__content">
                {advantages}
            </div>
        </section>
    );
}
