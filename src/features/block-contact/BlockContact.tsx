import { createElement } from "react";
import "./BlockContact.scss";
import { Contact } from "../../processes/types";
import { contacts } from "../../processes/data";
import Button from "../../shared/button/Button";

export default function BlockContact() {
    const el = createElement;

    const contact = contacts.map((key: Contact) => {
        return el(
            "div",
            {
                key: key.alt,
                className: "block-contact__content",
                style: {
                    alignItems: key?.addField ? "flex-start" : "center"
                }
            },
            [
                el(
                    "div",
                    {
                        className: "block-contact__icon",
                        key: key.alt
                    },
                    el("img", {
                        src: key.img,
                        alt: key.alt,
                    })
                ),
                el("div", {key: key.text}, [
                    el(
                        key.tag,
                        {...key.attr, key: key.alt + 123, className: "block-contact__text"},
                        key.text
                    ),
                    key?.addField ?
                        [
                            el("br", {key: key.alt + "br"}, null),
                            el(Button, {key: key.alt, ghost: true, modal: true, className:"block-contact__call-back"}, "Заказать звонок")
                        ] : null
                ])
            ]
        )
    });
    return (
        <section className="block-contact">
            <h5 className="block-contact__title">Контакты</h5>
            {contact}
        </section>
    )
}
