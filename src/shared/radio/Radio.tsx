import "./Radio.scss"
import { ChangeEvent } from "react";
import { RadioDto, RadioProps } from "../../processes/types";

export default function Radio({ radios, selected, onChange }: RadioProps) {
    const radiosList = radios.map((key: RadioDto) => {
        return (
            <label key={key.slide} className="radio__label">
                <input
                    type="radio"
                    name="radio-button"
                    value={key.slide}
                    checked={key.slide === selected.slide}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => onChange(event)}
                    disabled={key?.disabled}
                    className="radio__input"
                />

                <span className={`radio__checkmark ${key?.disabled ? "radio__checkmark_disabled" : ""}`} />
            </label>
        )
    });

    return (
        <div className="radio">
            {radiosList}
        </div>
    )
}
