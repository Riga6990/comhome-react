import "./Triangle.scss";
import { TriangleProps } from "../../processes/types";


export default function Triangle({rotate, className}: TriangleProps) {
  return (
    <div
        className={`triangle ${className || ""}`}
        style={{rotate: rotate ? `${rotate}deg` : "0deg"}}
    />
  )
}
