import "./Input.scss";
import Icon from "../icon/Icon";
import Point from "../../assets/icons/other/exclamation-point.svg";
import { FC } from "react";
import { InputProps } from "../../processes/types";


const Input: FC<InputProps> = ({
       type,
       value,
       name,
       placeholder,
       required,
       error,
       full,
       className,
       disabled,
       onChange,
   }) => {
    return (
        <div className="input">
            <input
                type={type ? type : "text"}
                value={value}
                name={name}
                required={required}
                placeholder={placeholder}
                onChange={onChange}
                disabled={disabled}
                className={
                    `input__field ${error ? "input__error" : ""} ${full ? "input__full-width" : ""} ${disabled ? "input__disabled" : ""} ${className || ""}`
                }
            />
            {error ? <Icon icon={Point} title="Внимание!!!" className="input__point" /> : null}
        </div>
    )
}

export default Input;
