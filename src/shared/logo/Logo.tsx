import React from "react";
import "./Logo.scss";
import Logotip from "../../assets/images/logo.svg";
import { LogoProps } from "../../processes/types";

export default function Logo({small}: LogoProps) {
  return (
      <div className="logo">
          <img
              src={Logotip}
              alt="Логотип"
              style={{"width": small ? "120px" : "142px"}}
          />
      </div>
  )
}
