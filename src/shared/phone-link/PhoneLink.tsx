import "./PhoneLink.scss";
import { PhoneLinkProps } from "../../processes/types";

export default function PhoneLink({className}: PhoneLinkProps) {
    return (
        <a href="tel:+79038010774" className={"phone-link " + className}>+ 7 (903) 801 07-74</a>
    )
}
