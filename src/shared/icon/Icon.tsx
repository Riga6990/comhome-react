import "./Icon.scss";
import { IconProps } from "../../processes/types";
export default function Icon({ icon, title, circle, rounded, square, rotate, className, disabled, onClick }: IconProps) {
  return (
     <button
        className={`
            icon 
            ${className || ''} 
            ${circle ? "icon__circle" : ""}
            ${rounded ? "icon__rounded" : ""}
            ${square ? "icon__square" : ""}
        `}
        style={{rotate: rotate ? `${rotate}deg` : "0deg"}}
        disabled={disabled}
        onClick={onClick}
     >
        <img src={icon} alt={title} style={{color: 'red'}}/>
     </button>
  )
}
