import "./Button.scss";
import { ButtonProps, ProviderState } from "../../processes/types";
import { Dispatch, MouseEvent, SetStateAction, useContext } from "react";
import { UserContext } from "../../App";

export default function Button({ children, disabled, type, onClick, small, ghost, modal, fullWidth, className }: ButtonProps ) {
    const { setState } = useContext<{ setState: Dispatch<SetStateAction<ProviderState>> }>(UserContext);
    const openModal = (): void => {
        setState((state: ProviderState): ProviderState => ({ ...state, modal: true }));
    };

    return (
        <button
            disabled={disabled}
            type={type ? type : "button"}
            className={`button ${className || ""} ${fullWidth ? "button__full-width" : ""} ${small ? "button__small" : ghost ? "button__ghost" : "button__default"}`}
            onClick={(e:MouseEvent<HTMLButtonElement>): void => {
                if (onClick) {
                    onClick(e);
                } else {
                    if (modal) openModal();
                }
            }}
        >
            {children}
        </button>
    );
}
