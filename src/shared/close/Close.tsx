import "./Close.scss";
import Icon from "../icon/Icon";
import CloseIcon from "../../assets/icons/other/close.svg";
import { Dispatch, SetStateAction, useContext } from "react";
import { CloseProps, ProviderState } from "../../processes/types";
import { UserContext } from "../../App";



export default function Close({className}: CloseProps) {
    const {setState} = useContext<{ setState: Dispatch<SetStateAction<ProviderState>> }>(UserContext);

    const closeWrapper = (): void => {
        setState((state: ProviderState): ProviderState => ({...state, sidebar: false, modal: false}));
    };

    return (
        <Icon
            icon={CloseIcon}
            className={className}
            onClick={closeWrapper}
        />
    )
}
