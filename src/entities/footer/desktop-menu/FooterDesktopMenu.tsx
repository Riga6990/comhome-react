import "./FooterDesktopMenu.scss";
import { ItemSubMenu, SubMenu, MenuProps } from "../../../processes/types";
import { Link } from "react-router-dom";

export default function FooterDesktopMenu({ menu }: MenuProps) {
    const list = menu.map((key: SubMenu) => {
        return (
            <li key={key.id} className="footer-desktop-menu__item">
                <Link to={key.path} className="footer-desktop-menu__link">
                    {key.title}
                </Link>
                {
                    key?.subMenu ? key.subMenu.map((item: ItemSubMenu) => {
                        return (
                            <ul key={item.id} className="footer-desktop-menu__submenu">
                                <li>
                                    <Link to={item.path} className="footer-desktop-menu__sublink">
                                        {item.title}
                                    </Link>
                                </li>
                            </ul>
                        )
                    }) : null
                }
            </li>
        )
    });

    return (
        <nav className="footer-desktop-menu">
            <ul className="footer-desktop-menu__column">{list}</ul>
        </nav>
    )
}
