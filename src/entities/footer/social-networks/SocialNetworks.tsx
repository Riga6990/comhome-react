import "./SocialNetworks.scss";
import Logo from "../../../assets/images/logo.svg";
import { socialNetwork } from "../../../processes/data";
import { SocialNetwork } from "../../../processes/types";

export default function SocialNetworks() {
    const listItems = socialNetwork.map((item: SocialNetwork, index: number) =>
        <a
            key={index}
            href={item.link}
            rel="noreferrer"
            target="_blank"
            className="social-networks__item"
        >
            <img src={item.icon} alt={item.text} />
        </a>
    );

    return (
        <div className="social-networks">
            <div><img src={Logo} alt="Логотип" /></div>
            <a href="tel:+79038010774" className="social-networks__link">+ 7 (903) 801 07-74</a>
            <ul className="social-networks__items">
                {listItems}
            </ul>
        </div>
    );
};
