import "./FooterMobileMenu.scss";
import { useState } from "react";
import { ItemSubMenu, SubMenu, MenuProps } from "../../../processes/types";
import { Link } from "react-router-dom";
import Icon from "../../../shared/icon/Icon";
import Triangle from "../../../assets/icons/other/triangle.svg";

export default function FooterMobileMenu({ menu }: MenuProps) {
    const [activeIds, setActiveIds] = useState<number[]>([]);
    const toggleMenu = (key: SubMenu): void => {
        if (!key.subMenu) return;
        const hasId = activeIds.some((item:number):boolean => item === key.id);
        if (hasId) {
            setActiveIds((currentArray: number[]) => currentArray.filter((item: number): boolean => item !== key.id));
        } else {
            setActiveIds((oldArray: number[]) => [...oldArray, key.id]);
        }
    };

    const flag = (id: number):boolean => {
        return activeIds.some((key: number) => key === id);
    };

    const list = menu.map((key: SubMenu) => {
        return (
            <li key={key.id} className="footer-mobile-menu__item" onClick={() => toggleMenu(key)}>
                <div className={flag(key.id) ? "footer-mobile-menu__wrap_open" : "footer-mobile-menu__wrap"}>
                    <Link to={key.path} className="footer-mobile-menu__link">{key.title}</Link>
                    {key?.subMenu ? <Icon icon={Triangle} rotate={90} className="footer-mobile-menu__triangle" /> : null}
                </div>

                {key?.subMenu ? key.subMenu.map((item: ItemSubMenu) => {
                    return (
                        <Link key={item.id} to={item.path} className={flag(key.id) ? "footer-mobile-menu__subitem" : "footer-mobile-menu__subitem_hidden"}>
                            {item.title}
                        </Link>
                    )}) : null
                }
            </li>
        )
    });

    return (
        <nav className="footer-mobile-menu">
            <ul className="footer-mobile-menu__row">{list}</ul>
        </nav>
    )
}
