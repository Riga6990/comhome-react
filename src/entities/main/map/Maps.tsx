import "./Maps.scss";
import { useState, useMemo } from "react";
import { YMaps, Map, Placemark } from "@pbe/react-yandex-maps";
import Pin from "../../../assets/icons/other/pin.svg";
import { coordinates } from "../../../processes/data";
import { MapState } from "../../../processes/types";
import useWindowDimensions from "../../../processes/helper";
import BlockContact from "../../../features/block-contact/BlockContact";
import Plus from "../../../assets/icons/other/plus.svg";
import Minus from "../../../assets/icons/other/minus.svg";
import Icon from "../../../shared/icon/Icon";

export default function Maps() {
    const width: number = useWindowDimensions().width;

    const [zoom, setZoom] = useState<number>(coordinates.zoom);

    const mapState = useMemo(
        (): MapState => ({
            center: width > 767 && width < 1024 ? coordinates.tabletCoordinates : coordinates.defaultCoordinates,
            zoom
        }),
        [zoom]
    );

    return (
        <YMaps>
            <div className="maps">
                <div className="container">
                    <div className="maps__block">
                        <BlockContact />
                    </div>
                </div>

                <Map
                    state={mapState}
                    width="100%"
                    className="maps__map"
                >
                    <div className="maps__buttons">
                        <Icon
                            icon={Plus}
                            onClick={(): void => {setZoom((zoom: number) => (zoom + 1))}}
                            className="maps__button"
                        />
                        <Icon
                            icon={Minus}
                            onClick={(): void => {setZoom((zoom: number) => (zoom - 1))}}
                            className="maps__button"
                        />
                    </div>

                    <Placemark
                        geometry={coordinates.defaultCoordinates}
                        options={{
                            iconLayout: "default#image",
                            iconImageHref: Pin,
                            iconImageSize: [60, 68]
                        }}
                    />
                </Map>
            </div>
        </YMaps>
    )
}
