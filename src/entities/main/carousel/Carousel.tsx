import "./Carousel.scss";
import { ChangeEvent, useEffect, useState } from "react";
import Button from "../../../shared/button/Button";
import Radio from "../../../shared/radio/Radio";
import { RadioDto } from "../../../processes/types";
import { carouselContent, listRadios } from "../../../processes/data";
import { CarouselContentEnum, CarouselItemsEnum, RadioEnum } from "../../../processes/constants";
import Toolbar from "../../../widgets/modal/Modal";

export default function Carousel() {

    const [carousel, setCarousel] = useState<RadioDto>({
        slide: RadioEnum.FirstSlide
    });

    const [title, setTitle] = useState<string>(carouselContent.title);

    const onChanged = (event: ChangeEvent<HTMLInputElement>): void => {
        setCarousel({...carousel, slide: event.target.value});
    };

    useEffect(() => {
        const totalSlides: number = Object.values(RadioEnum).length === 0 ? 0 : Object.values(RadioEnum).length - 1,
              currentSlide = Object.values(RadioEnum).findIndex((item: RadioEnum): boolean => item === carousel.slide),
              slide = Object.values(RadioEnum)[currentSlide === totalSlides ? 0 : (currentSlide + 1)],
              titleSlide = Object.values(CarouselContentEnum)[currentSlide === totalSlides ? 0 : (currentSlide + 1)];

        const interval = setInterval((): void => {
            setCarousel({
                ...carousel,
                slide: slide
            });

            setTitle(titleSlide)
        }, 4000)

        return () => clearInterval(interval);
    });

    return (
        <section className="carousel container">
            <div className="carousel__wrapper">
                <div className="carousel__container">
                    <div className="carousel__content">
                        <h1 className="carousel__title">{title}</h1>
                        <h2 className="carousel__description">{carouselContent.description}</h2>

                        <div className="carousel__items">
                            {carouselContent.items.map((item:CarouselItemsEnum) => {
                                return (<p key={item} className="carousel__item">{item}</p>)
                            })}
                        </div>

                        <div>
                            <Button modal>{carouselContent.button}</Button>
                            <Toolbar />
                        </div>
                    </div>

                    <div className={`carousel__background carousel__background_${carousel.slide}`} />
                </div>

                <div className="carousel__radio">
                    <Radio radios={listRadios} selected={carousel} onChange={onChanged} />
                </div>
            </div>
        </section>
    )
}



