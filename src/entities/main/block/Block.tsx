import "./Block.scss";
import { BlockProps } from "../../../processes/types";

export default function Block({ icon, title, description }: BlockProps ) {
    return (
        <div className="block">
            <img
                src={icon}
                alt={title}
                className="block__img"
            />
            <div>
                <h5 className="block__title">{title}</h5>
                <p className="block__description">{description}</p>
            </div>
        </div>
    );
}
