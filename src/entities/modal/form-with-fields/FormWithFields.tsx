import "./FormWithFields.scss";
import Input from "../../../shared/input/Input";
import Button from "../../../shared/button/Button";
import { FC, ChangeEvent, FormEvent, useState, useContext, Dispatch, SetStateAction } from "react";
import { modalContent as content, inputName } from "../../../processes/data";
import { ModalStatus } from "../../../processes/constants";
import { ProviderState, FormContact, ModalProps } from "../../../processes/types";
import { UserContext } from "../../../App";
import axios from "axios";

const apiUrl = "https://jsonplaceholder.typicode.com/posts";

const FormWithFields: FC<ModalProps> = ({handleStatus}: ModalProps) => {
    const { setState } = useContext<{setState: Dispatch<SetStateAction<ProviderState>>}>(UserContext);

    const [fields, setFields] = useState<FormContact>({
        name: "",
        phone: ""
    });

    const [disabled, setDisabled] = useState<boolean>(false);
    const [error, setError] = useState<boolean>(false);

    const handleNameChange = ({target}: ChangeEvent<HTMLInputElement>, field: string): void => {
        let data: string;
        const { value } = target,
              length = value.length;

        if (field === inputName.phone) {
            if (/[A-Za-zА-Яа-я@<>\][,./|\\*:%$#№!_=^`~&?]/.test(value)) return;
            if (length === 0 || length <= 4) {
                data = "+7 ("
            } else if (length === 7) {
                data = value + ") "
            } else if (length === 12) {
                data = value + " ";
            } else if (length === 15) {
                data = value + "-";
            } else if (length === 19) {
                return;
            } else {
                data = value;
            }
        } else {
            data = target.value;
        }
        setFields((state: FormContact) => ({
            ...state, [field]: data
        }));
    }

    const handleSubmit = (e: FormEvent<HTMLFormElement>): void => {
        e.preventDefault();
        const phone = fields.phone.length;
        if (phone !== 18) {
            setError(true);
        } else {
            setError(false);
            setDisabled(true);
            axios.post(apiUrl)
                .then((res): void => {
                    if (res?.status === 201) {
                        handleStatus(ModalStatus.Success);
                    } else {
                        handleStatus(ModalStatus.Error);
                    }
                })
                .catch((error): void => {
                    console.error("Ошибка при Заказе звонка: ", error);
                    handleStatus(ModalStatus.Error);
                })
                .finally((): void => {
                    setDisabled(false);
                    setTimeout((): void => {
                        setState((state: ProviderState): ProviderState => ({ ...state, modal: false }));
                        handleStatus(null);
                    }, 3000);
                });
        }
    }

    return (
        <form onSubmit={handleSubmit} className="form-with-fields">
            <Input
                value={fields.name}
                name={inputName.name}
                full
                disabled={disabled}
                placeholder={content.name}
                className="form-with-fields__field"
                onChange={(e) => handleNameChange(e, inputName.name)}
            />

            <Input
                type="tel"
                value={fields.phone}
                full
                disabled={disabled}
                name={inputName.phone}
                error={error}
                placeholder={content.phone}
                className="form-with-fields__field"
                onChange={(e) => handleNameChange(e, inputName.phone)}
            />

            <Button
                type="submit"
                disabled={disabled}
                fullWidth
            >
                {content.button}
            </Button>
        </form>
    )
}

export default FormWithFields;
