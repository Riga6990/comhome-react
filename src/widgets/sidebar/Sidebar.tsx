import "./Sidebar.scss";
import { useContext, useState } from "react";
import Wrapper from "../wrapper/Wrapper";
import { UserContext } from "../../App";
import { ProviderState } from "../../processes/types";
import { itemMenu } from "../../processes/data";
import { MenuDto, SubMenu, ItemSubMenu } from "../../processes/types";
import { Link } from "react-router-dom";
import Triangle from "../../shared/triangle/Triangle";

export default function Sidebar(): JSX.Element {
    const {state} = useContext<{ state: ProviderState }>(UserContext);

    const [toggle, setToggle] = useState(false);

    const handleClick = (item: MenuDto): void => {
        if (item?.subMenu) setToggle(!toggle);
    };

    const classSidebarItem = (item: MenuDto): string => {
        return `sidebar__link ${toggle && item?.subMenu ? "sidebar__link_open" : ""}`
    };

    const hasTriangle = (item: MenuDto) => {
        return (
            item?.subMenu ?
                <Triangle
                    rotate={toggle ? -90 : 90}
                    className={`sidebar__triangle ${toggle ? "sidebar__triangle_open" : ""}`}
                /> : null
        )
    };

    const links = itemMenu.map((key: MenuDto) => {
        if (key.id === 1) return null;
        return (
            <li key={key.id} className="sidebar__item" onClick={() => handleClick(key)}>
                <div className="sidebar__block">
                    <Link to={key.path} className={classSidebarItem(key)}>{key.title}</Link>
                    {hasTriangle(key)}
                </div>
                {
                    key?.subMenu ?
                        <nav className={toggle ? "sidebar__show" : "sidebar__hide"}>
                            {
                                key.subMenu.map((item: SubMenu) => {
                                    return (
                                        <ul key={item.id}>
                                            <li className="sidebar__subitem">
                                                <Link to={item.path} className="sidebar__sublink sidebar__sublink_main">
                                                    {item.title}
                                                </Link>
                                            </li>

                                            {
                                                item.subMenu.map((i: ItemSubMenu) => {
                                                    return (
                                                        <li key={i.id} className="sidebar__subitem">
                                                            <Link to={i.path} className="sidebar__sublink">
                                                                {i.title}
                                                            </Link>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul>
                                    )
                                })
                            }
                        </nav> : null
                }
            </li>
        )
    });

    return (
        <Wrapper isLogo open={state.sidebar}>
            <nav className="sidebar">
                <ul>{links}</ul>
            </nav>
        </Wrapper>
    )
}


