import "./Wrapper.scss";
import { useEffect, useRef, RefObject } from "react";
import { createPortal } from "react-dom";
import Logo from "../../shared/logo/Logo";
import Close from "../../shared/close/Close";
import { WrapperProps } from "../../processes/types";

const WrapperElement: HTMLElement | null = document.getElementById("wrapper");

export default function Wrapper({ isLogo, open, children }: WrapperProps) {
    let contentLogo = null;
    if (isLogo) contentLogo = <Logo small />;

    const dialog: RefObject<HTMLDialogElement>= useRef<HTMLDialogElement>(null);

    useEffect((): void => {
        if (open) {
            dialog.current?.showModal();
        } else {
            dialog.current?.close();
        }
    }, [open]);

    return createPortal(
        <dialog className="wrapper" ref={dialog}>
            <div className={`wrapper__header ${isLogo ? "wrapper__header_between" : "wrapper__header_end"}`}>
                {contentLogo}
                <Close className="wrapper__close" />
            </div>
            <div className="wrapper__container">
                {children}
            </div>
        </dialog>, WrapperElement!
    )
}
