import "./Footer.scss";
import FooterDesktopMenu from "../../entities/footer/desktop-menu/FooterDesktopMenu";
import FooterMobileMenu from "../../entities/footer/mobile-menu/FooterMobileMenu";
import SocialNetworks from "../../entities/footer/social-networks/SocialNetworks";
import { Link } from "react-router-dom";
import { itemMenu } from "../../processes/data";
import { MenuDto, SubMenu, ItemSubMenu } from '../../processes/types';
import useWindowDimensions from '../../processes/helper';

export default function Footer() {
    const width: number = useWindowDimensions().width;

    const copy: MenuDto[] = [...itemMenu];
    const service: ItemSubMenu = {
        id: copy[1].id,
        path: copy[1].path,
        title: copy[1].title
    };

    const menu: SubMenu[] = [
        // @ts-ignore
        ...copy[1].subMenu,
        service,
        copy[2],
        copy[3],
        copy[4],
    ];

    return (
        <footer className="footer">
            <section className="footer__container container">
                <div className="footer__block">
                    {width >= 1024 ? <FooterDesktopMenu menu={menu} /> : <FooterMobileMenu menu={menu} />}
                    <SocialNetworks />
                </div>
                <Link to="/" className="footer__privacy-policy">Политика конфиденциальности</Link>
            </section>
        </footer>
    );
}
