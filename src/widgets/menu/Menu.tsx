import "./Menu.scss";
import { itemMenu } from "../../processes/data";
import { MenuDto, SubMenu, ItemSubMenu } from "../../processes/types";
import Logo from "../../assets/images/logo.svg";
import { Link } from "react-router-dom";

export default function Menu() {
    const links = itemMenu.map((key: MenuDto) => {
        return (
            <li key={key.id} className="menu__main_item">
                {
                    key.id === 1 ?
                        <Link to={key.path}><img src={Logo} alt={key.title} /></Link> :
                        <>
                            <Link to={key.path} className="menu__main_link">{key.title}</Link>
                            {key?.subMenu ? <div className="menu__main_triangle"/> : null}
                            {
                                key?.subMenu ?
                                    <ul className="menu__dropdown">
                                        {
                                            key.subMenu.map((item: SubMenu) => {
                                                return (
                                                    <li key={item.id} className="menu__dropdown_item">
                                                        <div className="menu__dropdown_wrapper">
                                                            <Link to={item.path} className="menu__dropdown_link">
                                                                {item.title}
                                                            </Link>
                                                            <div className="menu__dropdown_triangle"/>
                                                        </div>

                                                        <ul className="menu__extra">
                                                            {item.subMenu.map((i: ItemSubMenu) => {
                                                                return (
                                                                    <li key={i.id} className="menu__extra_item">
                                                                        <Link to={i.path} className="menu__extra_link">
                                                                            {i.title}
                                                                        </Link>
                                                                    </li>
                                                                )
                                                            })}
                                                        </ul>
                                                    </li>
                                                )
                                            })
                                        }
                                    </ul> : null
                            }
                        </>
                }
            </li>
        )
    });
    return (
        <nav className="menu">
           <ul className="menu__main">{links}</ul>
        </nav>
    )
}
