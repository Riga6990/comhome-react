import "./Header.scss";
import { useContext, Dispatch, SetStateAction } from "react";
import MenuComponent from "../menu/Menu";
import Icon from "../../shared/icon/Icon";
import MenuIcon from "../../assets/icons/other/menu.svg";
import Phone from "../../assets/icons/other/phone.svg";
import Button from "../../shared/button/Button";
import PhoneLink from "../../shared/phone-link/PhoneLink";
import Sidebar from "../sidebar/Sidebar";
import { ProviderState } from "../../processes/types";
import { UserContext } from "../../App";


export default function Header():JSX.Element {
    const { setState } = useContext<{ setState: Dispatch<SetStateAction<ProviderState>> }>(UserContext);

    const openSidebar = (): void => {
        setState((state: ProviderState): ProviderState => ({ ...state, sidebar: true }));
    };

    return (
        <>
            <header className="header container">
                <section className="header__desktop">
                    <MenuComponent />
                    <div className="header__menu">
                        <PhoneLink className="header__phone" />
                        <Button small modal className="header__button">Заказать звонок</Button>
                        <Icon icon={Phone} title="Телефон" circle className="header__icon-phone" />
                    </div>
                </section>

                <section className="header__mobile">
                    <Icon icon={MenuIcon} title="Меню" circle onClick={openSidebar}/>
                    <PhoneLink className="header__phone" />
                    <Icon icon={Phone} title="Телефон" circle />
                </section>
            </header>

            <Sidebar />
        </>
    );
}
