import "./Modal.scss"
import { Dispatch, SetStateAction, useContext, useState } from "react";
import Wrapper from "../wrapper/Wrapper";
import FormWithFields from "../../entities/modal/form-with-fields/FormWithFields";
import { UserContext } from "../../App";
import { ProviderState } from "../../processes/types";
import Close from "../../shared/close/Close";
import { modalContent as content } from "../../processes/data";
import { ModalStatus } from "../../processes/constants";
import MessageSuccess from "../../assets/icons/messages/message-success.svg";
import MessageError from "../../assets/icons/messages/message-error.svg";


export default function Modal() {
    const { state } = useContext<{state: ProviderState, setState: Dispatch<SetStateAction<ProviderState>>}>(UserContext);

    const [status, setStatus] = useState<ModalStatus | null>(null);

    const changeStatus = (status: ModalStatus | null): void => {
        setStatus(status);
    };

    const statusState = () => {
        return (
            <div className="modal__status">
                <img
                    src={status === ModalStatus.Success ? MessageSuccess : MessageError}
                    alt={status === ModalStatus.Success ? content.success : content.error}
                />
                <p className="modal__text">
                    {status === ModalStatus.Success ? content.successText : content.errorText}
                </p>
            </div>
        )
    }

    const defaultState = () => {
        return (
            <div className="modal__block">
                <div className="modal__header">
                    <Close/>
                </div>

                <div className="modal__content">
                    <p className="modal__title">
                        {content.title}
                    </p>

                    <p className="modal__description">
                        {content.description}
                    </p>

                    <FormWithFields handleStatus={changeStatus} />

                    <p className="modal__info">
                        {content.info} <span className="modal__warning"> {content.warning}</span>
                    </p>
                </div>
            </div>
        )
    };
    return (
        <Wrapper open={state.modal}>
            <div className="modal">
                {
                    status ? statusState() : defaultState()
                }
            </div>
        </Wrapper>
    )
}
