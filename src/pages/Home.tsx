import Carousel from "../entities/main/carousel/Carousel";
import Maps from "../entities/main/map/Maps";
import BenefitsOfWork from "../features/benefits-of-work/BenefitsOfWork";

export default function Home() {
    return (
        <>
            <Carousel />
            <BenefitsOfWork />
            <Maps />
        </>
    )
}
