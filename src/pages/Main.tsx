import { Route, Routes } from "react-router-dom";
import { RouteName } from "../processes/constants";
import React from "react";

import Home from "./Home";
import About from "./About";
import Reviews from "./Reviews";
import Contacts from "./Contacts";
import NoPage from "./NoPage";

export default function ():JSX.Element {
  return (
      <main>
          <Routes>
              <Route index path={RouteName.Home} element={<Home/>} />
              <Route path={RouteName.About} element={<About />} />
              <Route path={RouteName.Reviews} element={<Reviews />} />
              <Route path={RouteName.Contacts} element={<Contacts />} />
              <Route path="*" element={<NoPage />} />
          </Routes>
      </main>
  )
}
