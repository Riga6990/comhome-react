import Empty from "../assets/images/404.png"

export default function NoPage() {
    return (
        <section className="no-page container">
            <img
                src={Empty}
                alt="такой страницы нет"
                className="container"
            />
        </section>
    )
}
