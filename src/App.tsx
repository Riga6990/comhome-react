import Header from "./widgets/header/Header";
import Main from "./pages/Main";
import Footer from "./widgets/footer/Footer";
import { createContext, useState } from "react";
import { providerState } from "./processes/data";
import { ProviderState } from "./processes/types";

export const UserContext = createContext<any>(null);

function App() {
    const [state, setState] = useState<ProviderState>(providerState);

    return (
        <UserContext.Provider value={{state: state, setState: setState}}>
            <Header/>
            <Main/>
            <Footer/>
        </UserContext.Provider>
    );
}

export default App;
